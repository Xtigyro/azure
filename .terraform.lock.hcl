# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version = "3.6.0"
  hashes = [
    "h1:icNPivGM5+Rilvii04ygDEycpSGRzQ4oH+4oXKfhd8M=",
    "zh:08a915f21564737090eea015f47d7dd5c2d1a5f63b49211937534f643f47b8ca",
    "zh:145527bed51ec8070a85e967395fcd9aa32c97fbf2cf42747397cef6f1388213",
    "zh:1e3c8c26af5076b9067ac9f6f71b188c25838db42e8d6c11128322d6c4655982",
    "zh:5a795890e78121f7b8d8a3ae6fdc6a6f69083465b6a2073c9c0f0564dad4f7ec",
    "zh:765219c271e8af16fd436cf88e32079379b035d92bca0df556dee9adf9c9152f",
    "zh:7804d99ba61f74db1a4a911d181e7f8cee62c41658fe424cfa28362d6bbe8784",
    "zh:85e51fd6402de3e842e75ea02dc79cc3903fd030bd3d312bfcb78a8a61b8dfaa",
    "zh:8b13a75dcd932dc43616d058eeae60fada70bc93b0deb7152a4aba55d365e5b0",
    "zh:8d80014cc5acf9c4c64cd73e8f15b418b52cc54e112b1ebb49fdefa2ad8e13eb",
    "zh:b25129758f8aa6835dd22101f945abed6430d457659d20d2638eff27ca71bddd",
    "zh:b35e089343a6185db0196273517fcfe98fbd13d6ac1901b49d1d24b2b39b2ff1",
    "zh:cd928bc0e5d42fb966fd583cb8c46169cf80d35a57859db949d0c0b802fef35d",
    "zh:f8af5be58d41afcda522926e3768728fe412a87dbacca7ecb02341b854bf798f",
  ]
}

provider "registry.terraform.io/hashicorp/azurerm" {
  version = "2.62.1"
  hashes = [
    "h1:QhP6grVYdqzdeEl9kW3waWDXpGqO7q0pDfyBVU9fois=",
    "zh:4a810e2ff90219efdc26234b5ac2171d97dfa31dfd465e73ff24fdea3bcbf745",
    "zh:61af756776c24ae79949724f83f4b39582cfdcfc67c8806ff6a39c340acc7a50",
    "zh:6b9042c511f4499ef3cb38730bb781b6a98e52d6491b34d9030f2658c78486ac",
    "zh:6ff6b736339302623272869a6f4e2ed5dfad0975227addee09495de83e878c6e",
    "zh:7ff411ff41e07dacb54b742e0645960555d5a783e868cfe427c79fcff6e1a3b1",
    "zh:8e316717334727cb424a9bc86624fb3a9dbf1389658dfa316007221d11bb45db",
    "zh:c64b1114b928c1c352d80ccac81244bada31c03522606bd4098b85ff4e78f00b",
    "zh:ca52ab530c0e7a9fc32342149e26bf632f9f1a3019c720dff7ce0721e3c220e3",
    "zh:d03e0a3cca4b8c155db62f06712e77b344289e5e8ed16ff9213fb8b496d842c3",
    "zh:d9f0b71ab5d34c9ff8b237ab4317e4f6eaa0306599154940495f303c3eac2e7b",
    "zh:ed70b51555cdcf3f91227cf64d229d78cbed137c50b138226d1f39697e9ebb3f",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version = "2.3.1"
  hashes = [
    "h1:uIwQjQesidbjSS5kMLh39b7ZEwTbKoxSCT0xbDNpfpA=",
    "zh:05bc0756996f49e8db644b4da63df42a24bfa3fadc5f7bbc3932cb364d0e0310",
    "zh:14c5b75133a1e0bcb100b94eeec37cdadafb85f89f7bd6d3f870cbe023848b31",
    "zh:2540962d1f9843b9f806592b84c6b16dea2dc64438911e496e9284a579d4735b",
    "zh:40ca8bb8f86d7f0be9457c61c82a1e68a3f90b585b45f3009ed4c2f66a79dc1c",
    "zh:475409c1c31d2ef61701a0ffb26c01df41a2709411eedae138c8a470bc670107",
    "zh:600b5118485a73262aa33fae3367341c822fd5842b64d9b85a43e9a75a2d3f20",
    "zh:70efb8882f13eb1e703b6dbfcf05ac2a904a69be3a4e7e1378254df63ff51a66",
    "zh:8211a1616f044862a40ab242891790d2a0588d13672b6f3d6aff35b816232f4a",
    "zh:829e24ec14bb6a2799393dae9da513dbd8074c7a1633e9aa90c415a50fa37de9",
    "zh:93ea75e2db7c8eac2b0217eab2a2bedc4f330da5749f3b575aabbbdcf4dd92ca",
    "zh:ad05540d691fd6ba06bf3faa11dd2f754231646e801c11ab0f235ca854cd21fb",
  ]
}

provider "registry.terraform.io/terraform-providers/gitlab" {
  version     = "3.5.0"
  constraints = ">= 2.9.0"
  hashes = [
    "h1:OPAh3smkOF5k4fkr4pDrKFBfFgx+oGq96Zvn+HEpNRs=",
    "zh:20a7e10d88020a88f1aedd93d1de9a70efcc192f5c630df6a7818c4c3e501ac5",
    "zh:30c2f168869e7b243c844610bba5964de4acec9d0bb1255fc1ee6b97645dc398",
    "zh:4429f353f907f496997590113f1552db153d1b5793a64b06dd30ddd69e4c5c9b",
    "zh:9ab8c83053e8af0722e2aa04bc676a40543cc12f82e250d711bfaf2a791b58b1",
    "zh:a51dacd1f2022ac3edada0541327b87a7886ad981c32d29fd0bb48139a0b1360",
    "zh:ab568a4a19e272f2ead4d6e4f2a8ae61a18a952c92de741dfa6b1f501a18d30c",
    "zh:c7a7440c9b4698524506a6cbf8c8daf0dae32af3e245882b1ae2217c5a53bb9d",
    "zh:cc344adba161de020c677f7a6e233d829ffdfcd80ca154633caa5b5f23735346",
    "zh:ce6f02db7da21c308e824ea3d051e182f310fdba74fd4d40fedceb539b97daed",
    "zh:e44c8cde080e8690533623d4c56d9070cb4c720d12774e341c5b75c20291cd2d",
    "zh:ebdc5b7c23f3a0be2776402782312277cbd5ec5b54b8302eb64934e96e6c9a28",
  ]
}
